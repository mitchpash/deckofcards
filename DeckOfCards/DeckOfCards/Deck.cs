﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DeckOfCards
{
    // When a new deck is created, you’ll create a card of each rank for each suit and add them to the deck of cards, 
    //      which in this case will be a List of Card objects.
    //
    // A deck can perform the following actions:
	//     void Shuffle() -- Merges the discarded pile with the deck and shuffles the cards
	//     List<card> Deal(int numberOfCards) - returns a number of cards from the top of the deck
	//     void Discard(Card card) / void Discard(List<Card> cards) - returns a card from a player to the 
	//         discard pile	
    // 
    // A deck knows the following information about itself:
	//     int CardsRemaining -- number of cards left in the deck
	//     List<Card> DeckOfCards -- card waiting to be dealt
    //     List<Card> DiscardedCards -- cards that have been played
    class Deck : IEnumerable
    {
        //
        public int CardsRemaining => ListOfActiveCards.Count();
        private readonly Random _rng = new Random();
        //List of total possible cards in circulation (Standard Deck of 52 Cards)
        public List<Card> ListOfCards { get; set; } = new List<Card>();
        //List of Active Cards (Deal-able Deck)
        public List<Card> ListOfActiveCards { get; set; } = new List<Card>(); 
        //List of Inactive Cards (Discard Pile)
        public List<Card> ListOfInactiveCards { get; set; } = new List<Card>();
        /// <summary>
        /// Create a new ListOfCards that represents a new standard deck of 52 cards, without Jokers.
        /// </summary>
        public Deck()
        {
            //Full Version
            //var fullStackOfCards = new List<Card>();
            //foreach (var suit in Enum.GetNames(typeof(Card.Suit)))
            //{
            //    foreach (var rank in Enum.GetNames(typeof(Card.Rank)))
            //    {
            //        fullStackOfCards.Add(new Card(suit, rank));
            //    }
            //}
            //ListOfCards = fullStackOfCards;

            //LINQ Expression
            ListOfCards = (from suit in Enum.GetNames(typeof(Card.Suit)) from rank in Enum.GetNames(typeof(Card.Rank)) select new Card(suit, rank)).ToList();
        }

        public Deck(int numberOfDecks)
        {
            
        }
        
        public List<Card> Deal(int numberOfCards)
        {
            if (CardsRemaining < numberOfCards)
            {
                var tempListOfCards = ListOfActiveCards.Take(numberOfCards).ToList<Card>();
                foreach (var card in tempListOfCards)
                {
                    ListOfInactiveCards.Add(card);
                    ListOfActiveCards.Remove(card);
                }
                return tempListOfCards;
            }
            else
            {
                return new List<Card>();
            }
        }

        public void Discard(Card card)
        {

        }

        public void Discard(List<Card> listOfCards)
        {

        }
        /// <summary>
        /// Merges the discarded pile with the deck and shuffles the cards.
        /// </summary>
        public List<Card> Shuffle()
        {
            //Full Version
            //var tempListOfCards = new List<Card>();
            //foreach (var card in ListOfCards)
            //{
            //    int randomCardIndex = _rng.Next(ListOfCards.Count + 1);
            //    tempListOfCards.Add(ListOfCards[randomCardIndex]);
            //}
            //ListOfCards = tempListOfCards;

            //Linq Expression
            return ListOfCards.Select(card => _rng.Next(ListOfCards.Count)).Select(randomCardIndex => ListOfCards[randomCardIndex]).ToList();
        }

        //private Deck enumerator class
        private class DeckEnumerator : IEnumerator
        {
            private readonly List<Card> _deckOfCards;
            int _position = -1;

            public DeckEnumerator(List<Card> list)
            {
                _deckOfCards = list;
            }

            public IEnumerator GetEnumerator()
            {
                return (IEnumerator)this;
            }

            //IEnumerator MoveNext
            public bool MoveNext()
            {
                _position++;
                return (_position < _deckOfCards.Count);
            }

            //IEnumerator Reset
            public void Reset()
            { _position = -1; }

            //IEnumerator Current in Try Catch
            public object Current
            {
                get
                {
                    try
                    {
                        return _deckOfCards[_position];
                    }
                    catch (IndexOutOfRangeException)
                    {
                        throw new InvalidOperationException();
                    }
                }
            }
        }  //end nested class
        public IEnumerator GetEnumerator()
        {
            return new DeckEnumerator(ListOfCards);
        }
    }
    
}
