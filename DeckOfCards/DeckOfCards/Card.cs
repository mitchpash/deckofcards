﻿using System;

namespace DeckOfCards
{
    // What makes a card?
    // A card is comprised of it’s suit and its rank.  Both of which are enumerations.
    // These enumerations should be "Suit" and "Rank"
    class Card
    {
        public enum Suit
        {
            Hearts,
            Diamonds,
            Clubs,
            Spades
        }
        public enum Rank
        {
            Two,
            Three,
            Four,
            Five,
            Six,
            Seven,
            Eight,
            Nine,
            Ten,
            Jack,
            Queen,
            King,
            Ace
        }
        public Suit CardSuit { get; set; }
        public Rank CardRank { get; set; }
        public Card(string cardSuit, string cardRank)
        {
            CardSuit = (Suit)Enum.Parse(typeof(Suit), cardSuit);
            CardRank = (Rank)Enum.Parse(typeof(Rank), cardRank);
        }

        public override string ToString()
        {
            return this.CardRank.ToString() + " of " + this.CardSuit.ToString();
        }
    }
}